**Adidas Subscription System**

**Contents**
- Microservices 
- Pre requisites to setup microservices on local (Manually)
- Tools and Technologies used
- Steps to start microservices on local (Manually)
- Swagger urls 
- TDD using Mockito
- Steps to start microservices on local using Docker containers
- Jenkins Pipeline
- Kubernetes setup
- Screenshots

**Microservices:**

1. authentication-service: This is the entry point for the microservices landscape. It acts as a API Gateway whose task is to perform authentication and then route the valid request to subscription service.

2. subscription-service : API Gateway sends the validated request to subscription service which peforms the CRUD operations related to subscriptions and calls the notification service to send out the notifications. The subscripton data is persisted in Mysql database.

3. notification-service : A mock service created to send out the notifications

![Capture](/uploads/641494d9fa0e17c781605874b2695944/Capture.PNG)

**Pre requisites to setup microservices on local (Manually)**:

1. java 8
2. maven 3.x.x (3.6.3 - _in my case_)
3. mysql 8.0 run the attached script 
[data-mysql.sql](/uploads/b9c83435920554f260c87535d70c5946/data-mysql.sql)
4. RabbitMQ with userName 'adidas' and password 'adidas' should be created along with virtual host permissions(_Refer to Screenshots section below_)

**Tools and Technologies used:**

1. API Gateway - Netflix Zuul library
2. Authentication - using JWT tokens
3. ORM - Hibernate
4. Springboot for services
5. TDD using Mockito framework
6. Build tool - Maven
7. CI/CD - Jenkins pipeline
8. containerization tool - Docker
9. conatiner orchestration - Kubernetes
10. RabbitMQ for message queuing


**Steps to start microservices on local (Manually)**:

1. Checkout all the three micrservices from git:

        https://gitlab.com/gunjitanand/authentication-service.git
        https://gitlab.com/gunjitanand/notification-service.git
        https://gitlab.com/gunjitanand/subscription-service.git


2. mvn clean install in all the three microservices

3. java -jar target/<_service-name_>-service-0.0.1-SNAPSHOT.jar to start the services

**Testing services locally**:

1. **authentication-service**
      Generate the JWT token in postman or using below curl command:

  curl --location --request POST 'http://localhost:8082/authenticate' \
--header 'Content-Type: application/json' \
--data-raw '{
       "userName":"adidas",
       "password":"adidas"
}'

2. **subscription-service**
     This can be accesed using JWT token in api gateway or without the token directly **(only for local testing purpose)**.

  **Using JWT token**:

   curl --location --request POST 'http://localhost:8082/subscription/create' \
   --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmb28iLCJleHAiOjE2MjQxMDg4NzQsImlhdCI6MTYyNDA3Mjg3NH0.   avql676mecp3IvKsysa9BvUnUiaXXa_-fuC0u4KlcW4' \
   --header 'Content-Type: application/json' \
   --data-raw '{
  "consentFlag": "Y",
  "email": "abcd@gmail.com",
  "firstName": "abcd",
  "gender": "F",
  "newsLetterId": 1
}'     

  **Without JWT token**:

  curl --location --request POST 'http://localhost:8080/subscription/create' \
--header 'Content-Type: application/json' \
--data-raw '{
  "consentFlag": "Y",
  "email": "abcd@gmail.com",
  "firstName": "abcd",
  "gender": "F",
  "newsLetterId": 1
}'

3. **notification-service**:

curl --location --request POST 'http://localhost:8081/notification/create' \
--header 'Content-Type: application/json' \
--data-raw '{
  "consentFlag": "Y",
  "email": "abcd@gmail.com",
  "firstName": "abcd",
  "gender": "F",
  "newsLetterId": 1
}'

**Swagger urls**:

http://localhost:8080/swagger-ui.html

http://localhost:8081/swagger-ui.html


**TDD using Mockito:**
_SubscriptionServiceTest.java_ (subscription service)


**Steps to start microservices on local using Docker containers**:


1. Create Docker network

   _docker network create adi_

2. Run MySQL database container

   _docker run -d --name=mysql --network adi --network-alias mysql -e MYSQL_ROOT_PASSWORD=Acc123##  -e MYSQL_DATABASE=adidas -p3306:3306  -v /mnt/mysql-data:/var/lib/mysql mysql/mysql-server:latest_

3. Run RabbitMQ Container

  _docker run -d --name rabbitmq --hostname my-rabbit --network adi -e RABBITMQ_DEFAULT_USER=adidas -e RABBITMQ_DEFAULT_PASS=adidas -p 5673:5672 -p 15672:15672 rabbitmq:management_

4. Clone git repository and build docker image
  
    1. **Notification service**

        git clone https://gitlab.com/gunjitanand/notification-service.git

        cd notification-service

        mvn clean install

        docker build -t notification-service --no-cache .

        docker run -d -p7777:8081 notification-service:latest

    2. **Subscription service**

        git clone https://gitlab.com/gunjitanand/subscription-service.git

        cd subscription-service

        mvn clean install

        docker build -t subscription-service --no-cache .

        docker run -d -p6666:8080 --network adi subscription-service:latest

 
    3. **Authentication service**

        git clone https://gitlab.com/gunjitanand/authentication-service.git

        cd authentication-service

        mvn clean install

        docker build -t authentication-service --no-cache .
   
        docker run -d -p5555:8082 --network adi authentication-service:latest





**Jenkins Pipeline**

  **Configure**
  
      1. Download Jenkins, this was tested with 2.21[http://mirrors.jenkins-ci.org/war/2.21/jenkins.war].

      2. Start Jenkins: JENKINS_HOME=~/.jenkins java -jar ~/Downloads/jenkins-2.21.war 

      3. Create First Admin User, Save and Finish.

      4. Install suggested plugins

      5. Manage Jenkins, Global Tool Configuration, configure Maven, use name M3 (this name is used in Jenkinsfile)

      6. Manage Jenkins, Manage Plugins, Available, install ocker Pipeline plugin, Install without restart, select Restart Jenkins

  **Create Project**
  
      1. Create a new project of the type Pipeline

      2. Configure git repo
      
      3. Build Now

**Kubernetes setup**
All the configuration files related to Kubernetes are present in the below location:

https://gitlab.com/gunjitanand/kube-config-files

**Screenshots**

[snapshots_for_reference.docx](/uploads/f2f053fba96ab4dbd70cb185391fa924/snapshots_for_reference.docx)

