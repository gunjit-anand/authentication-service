package com.adidas.authservice.service;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService{

	//this is hard coded for the moment but alternatively encrypted password can be stored in db
	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		return new User("adidas", "$2a$10$HFjgrsKlJ3jTEX998R.EwetoTc6pdcQzc0FVdceJCKhXopkxGjbMO", new ArrayList<>());
	}

}
