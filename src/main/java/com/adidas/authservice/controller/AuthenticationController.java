package com.adidas.authservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adidas.authservice.model.AuthenticationRequest;
import com.adidas.authservice.service.MyUserDetailsService;
import com.adidas.authservice.util.JwtTokenUtil;

@RestController
public class AuthenticationController {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(AuthenticationController.class);

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	private MyUserDetailsService myUserService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> generateToken(@RequestBody AuthenticationRequest request) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));
		} catch (BadCredentialsException ex) {
			LOGGER.debug("Incorrect Username or Password");
			throw new Exception("Incorrect Username or Password");
		}
		UserDetails userdetails = myUserService.loadUserByUsername(request.getUserName());
		return ResponseEntity.ok(jwtTokenUtil.generateToken(userdetails));
	}
	
	
}
